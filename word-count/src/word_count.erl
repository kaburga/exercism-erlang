-module(word_count).

-export([count_words/1]).

count_words(Sentence) ->
    Words = prepare_words(Sentence),
    Inc = fun(V) -> V + 1 end,
    Word_To_Map = fun(W, Map) ->
                          maps:update_with(W, Inc, 1, Map)
                  end,
    lists:foldl(Word_To_Map, maps:new(), Words).

prepare_words(Sentence) ->
    TrimChars = ",:!&@$%^.'" ++ [$\n],
    UntrimmedWords = string:lexemes(
                       string:lowercase(Sentence),
                       " ,"),
    lists:map(fun(Str) -> string:trim(Str, both, TrimChars) end, UntrimmedWords).
