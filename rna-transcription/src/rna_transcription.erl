-module(rna_transcription).

-export([to_rna/1]).


to_rna(Strand) -> 
    Result = lists:map(fun(Ch)->
                               case Ch of
                                   $G -> $C;
                                   $C -> $G;
                                   $T -> $A;
                                   $A -> $U;
                                   _  -> Ch 
                               end
                       end,
                       Strand),
    Result.

