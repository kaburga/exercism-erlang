-module(grains).

-export([square/1, total/0]).


square(Square) when is_integer(Square) andalso Square >= 1 andalso Square =< 64 ->
    Sum = math:pow(2, (Square-1)),
    erlang:list_to_integer(erlang:float_to_list(Sum, [{decimals,0}]));
square(_Square) ->
    {error, "square must be between 1 and 64"}.

total() ->
    total(64, 0).

total(0, Sum) ->
    Sum;
total(N, Sum) ->
    total(N-1, Sum+square(N)).

