-module(roman_numerals).

-export([roman/1]).

roman(0) -> [];
roman(Number) when Number >= 1000 ->
    N = Number div 1000,
    encode(N, $M, 0, 0) ++ roman(Number rem 1000);
roman(Number) when Number >= 100 ->
    N = ((Number div 100) rem 10),
    encode(N, $C, $D, $M) ++ roman(Number rem 100);
roman(Number) when Number >= 10 ->
    N = ((Number div 10) rem 10),
    encode(N, $X, $L, $C) ++ roman(Number rem 10);
roman(Number) when Number >= 1 ->
    N = (Number rem 10),
    encode(N, $I, $V, $X).

encode(0, _, _, _) -> [];
encode(1, I, _, _) -> [I];
encode(2, I, _, _) -> [I, I];
encode(3, I, _, _) -> [I, I, I];
encode(4, I, V, _) -> [I, V];
encode(5, _, V, _) -> [V];
encode(6, I, V, _) -> [V, I];
encode(7, I, V, _) -> [V, I, I];
encode(8, I, V, _) -> [V, I, I, I];
encode(9, I, _, X) -> [I, X].
