-module(all_your_base).

-export([rebase/3]).

rebase(_Digits, InputBase, _OutputBase) when InputBase < 2  -> {error, "input base must be >= 2"};
rebase(_Digits, _InputBase, OutputBase) when OutputBase < 2 -> {error, "output base must be >= 2"};
rebase([], _InputBase, _OutputBase) -> {ok, [0]};
rebase(Digits, InputBase, OutputBase) ->
    %%Not_In_Bounds = lists:max(Digits) >= InputBase orelse lists:min(Digits) < 0,
    N = to_base_ten(Digits, InputBase),
    case N of
        {error, _} -> {error, "all digits must satisfy 0 <= d < input "
                       "base"};
        _ -> {ok, to_dst_base(OutputBase, N)}
    end.

to_base_ten(Digits, InputBase) ->
    Len = erlang:length(Digits),
    to_base_ten(Digits, [], Len-1, InputBase).

to_base_ten([H|_], _Res, _Len, InputBase) when (H >= InputBase) orelse (H < 0) ->
    {error, "all digits must satisfy 0 <= d < input "
     "base"};
to_base_ten([H|T], Res, Len, InputBase) ->
    Item = H * erlang:trunc(math:pow(InputBase, Len)),
    to_base_ten(T, [Item]++Res, Len-1, InputBase);
to_base_ten([], Res, _Len, _InputBase) ->
    lists:foldl(fun(X, Sum) -> X + Sum end, 0, Res).

to_dst_base(OutputBase, N) -> to_dst_base(OutputBase, [], N).
to_dst_base(OutputBase, Res, N) ->
    if
        N < OutputBase -> [N] ++ Res;
        true           -> to_dst_base(OutputBase, [N rem OutputBase]++Res, erlang:trunc(N/OutputBase))
    end.
