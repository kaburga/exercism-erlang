-module(isbn_verifier).

-export([is_valid/1]).


is_valid(IsbnString) ->
    IsbnNumber = re:replace(IsbnString, "-", "", [global, {return,list}]),
    valid_format(IsbnNumber) andalso valid_checksum(IsbnNumber).

valid_format(Isbn) ->
    nomatch =/= re:run(Isbn, "^[0-9]{9}[0-9X]$").

valid_checksum(Isbn) ->
    sum(preprocess(Isbn)) rem 11 =:= 0.

preprocess(Isbn) ->
    Multipliers = lists:reverse(lists:seq(1, 10)),
    [begin {X,N}=P, to_integer(X)*N end || P <- lists:zip(Isbn, Multipliers)].

to_integer($X) ->
    10;
to_integer(N) ->
    {Digit, _}=string:to_integer([N]),
    Digit.

sum(Isbn) ->
    lists:foldl(fun(Num, Sum)-> Num+Sum end, 0, Isbn).
