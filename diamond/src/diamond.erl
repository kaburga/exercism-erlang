-module(diamond).

-export([rows/1]).

-define(Alphabet, lists:reverse(lists:seq($A, $Z))).

rows("A") -> ["A"];
rows(Letter) ->
    DiamondLetters = string:find(?Alphabet, Letter),
    Length = string:length(DiamondLetters),
    make_diamond(DiamondLetters, Length).

make_diamond([Letter|DiamondLetters], CenterPadding) ->
    Middle = make_diamond_middle(Letter, CenterPadding),
    TopHalf = make_diamond_half(DiamondLetters, CenterPadding),
    BottomHalf = lists:reverse(TopHalf),
    BottomHalf ++ Middle ++ TopHalf ++ [].

make_diamond_middle(Letter, RightPadding) ->
    make_diamond_part(Letter, 0, RightPadding-2).

make_diamond_half(DiamondLetters, RightPadding) ->
    make_diamond_half(DiamondLetters, 1, RightPadding-3).

make_diamond_half("A", Padding, -1) ->
    PadSpace = lists:duplicate(Padding, $ ),
    [unicode:characters_to_list([PadSpace, "A", PadSpace])];
make_diamond_half([Letter|DiamondLetters], LeftSpace, RightSpace) ->
    make_diamond_part(Letter, LeftSpace, RightSpace) ++ make_diamond_half(DiamondLetters,
                                                                          LeftSpace+1,
                                                                          RightSpace-1).
make_diamond_part(Letter, LeftSpace, RightSpace) ->
    Side = [lists:duplicate(LeftSpace, $ ),
            Letter,
            lists:duplicate(RightSpace, $ )],
    Middle = " ",
    [unicode:characters_to_list([Side,
                                 Middle,
                                 string:reverse(Side)])].
