-module(rational_numbers).

-export([absolute/1, add/2, divide/2, exp/2, mul/2, reduce/1, sub/2]).

absolute({A1, B1}=_R) ->
    A = erlang:abs(A1),
    B = erlang:abs(B1),
    reduce({A, B}).

add({A1, B1}=_R1, {A2, B2}=_R2) ->
    A = (A1*B2)+(A2*B1),
    B = B1*B2,
    reduce({A, B}).

divide({A1, B1}=_R1, {A2, B2}=_R2) ->
    A = A1 * B2,
    B = A2 * B1,
    reduce({A, B}).

exp(Base, {A1, B1}=_Exponent) ->
    math:pow(Base, A1/B1);
exp({A1, B1}=_Base, Exponent) when erlang:is_float(Exponent) ->
    A = math:pow(A1, Exponent),
    B = math:pow(B1, Exponent),
    A/B;
exp({A1, B1}=_Base, Exponent) when Exponent < 0 ->
    A = erlang:trunc(math:pow(B1, erlang:abs(Exponent))),
    B = erlang:trunc(math:pow(A1, erlang:abs(Exponent))),
    reduce({A, B});
exp({A1, B1}=_Base, Exponent) when Exponent >= 0 ->
    A = erlang:trunc(math:pow(A1, Exponent)),
    B = erlang:trunc(math:pow(B1, Exponent)),
    reduce({A, B}).

mul({A1, B1}=_R1, {A2, B2}=_R2) ->
    A = A1*A2,
    B = B1*B2,
    reduce({A, B}).

reduce({A, B}=_R) ->
    GCD = gcd(A,B),
    case B < A andalso B < 0 of
        true -> {A div -GCD, B div -GCD};
        false  -> {A div GCD, B div GCD}
    end.

sub({A1, B1}=_R1, {A2, B2}=_R2) ->
    A = (A1 * B2 - A2 * B1),
    B = (B1 * B2),
    reduce({A, B}).

%% Helper functions
gcd(0, B) -> erlang:abs(B);
gcd(A, 0) -> erlang:abs(A);
gcd(A, B) -> gcd(B, A rem B).
