-module(custom_set).

-export([add/2, contains/2, difference/2, disjoint/2, empty/1, equal/2, from_list/1, intersection/2, subset/2,
         union/2]).


add(Elem, Set) ->
    case contains(Elem, Set) of
        true  -> Set;
        false -> [Elem] ++ Set
    end.

contains(Elem, Set) ->
    lists:member(Elem, Set).

difference(Set1, Set2) ->
    Set1 -- Set2.

disjoint(Set1, Set2) ->
    lists:all(fun(Elem) ->
                      not contains(Elem, Set2)
              end, Set1).

empty([]) -> true;
empty(_Set) -> false.

equal(Set1, Set2) ->
    lists:sort(Set1) =:= lists:sort(Set2).

from_list(List) ->
    from_list(List, []).

from_list([], Set) ->
    Set;
from_list([H|T], Set) ->
    from_list(T, add(H, Set)).

intersection(Set1, Set2) ->
    lists:filter(fun(Elem) ->
                         contains(Elem, Set2)
                 end, Set1).

subset([], _Set2) ->
    true;
subset(Set1, Set2) ->
    lists:all(fun(Elem) ->
                      contains(Elem, Set2)
              end, Set1).

union(Set1, Set2) ->
    lists:foldl(fun(Elem, AccSet) ->
                        add(Elem, AccSet)
                end, Set2, Set1).
