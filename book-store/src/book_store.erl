-module(book_store).

-export([total/1]).

-define(BookSeries, [1,2,3,4,5]).
-define(Price, 800).

total([]) -> 0;
total(Basket) ->
    total_with_discount(lists:sort(Basket), 0).

total_with_discount([], Total) ->
    erlang:trunc(Total);
total_with_discount(Basket, Total) when erlang:length(Basket) == 12 ->
    case erlang:length(lists:usort(Basket)) == 5 of
        true ->
            FivesAndTwoTotal = (2 * ((?Price * 5)*0.75)) + ((2* ?Price)*0.95),
            NewTotal = Total + FivesAndTwoTotal,
            total_with_discount([], NewTotal);
        false ->
            do_discount_sum(Basket, Total)
    end;
total_with_discount(Basket, Total) when erlang:length(Basket) == 4
                                        orelse erlang:length(Basket) == 8
                                        orelse erlang:length(Basket) == 16->
    case erlang:length(lists:usort(Basket)) >= 4 of
        true ->
            GroupsOfFour = erlang:length(Basket) div 4,
            NewTotal = Total + (GroupsOfFour * ((?Price * 4)*0.80)),
            total_with_discount([], NewTotal);
        false ->
            do_discount_sum(Basket, Total)
    end;
total_with_discount(Basket, Total) ->
    do_discount_sum(Basket, Total).

do_discount_sum(Basket, Total) ->
    {NewBasket, DiscountTotal} = discount(Basket, ?BookSeries, 0, 0),
    total_with_discount(NewBasket, Total+DiscountTotal).

discount(Basket, [], Total, Discount) ->
    case Discount of
        0 -> {erlang:tl(Basket), ?Price};
        1 -> {Basket, Total};
        2 -> {Basket, Total*0.95};
        3 -> {Basket, Total*0.90};
        4 -> {Basket, Total*0.80};
        5 -> {Basket, Total*0.75}
    end;
discount(Basket, [B|Bs], Total, Discount) ->
    case lists:member(B, Basket) of
        true ->
            NewBasket = Basket -- [B],
            discount(NewBasket, Bs, Total+?Price, Discount+1);
        false ->
            discount(Basket, Bs, Total, Discount)
    end.
