-module(armstrong_numbers).

-export([is_armstrong_number/1]).


is_armstrong_number(Number) ->
    DigitList = [begin {Int, _} = string:to_integer([D]), Int end || D <- erlang:integer_to_list(Number)],
    NumOfDigits = erlang:length(DigitList),
    ResArmstrongCalc = lists:foldl(fun(D, Acc)-> Acc + math:pow(D, NumOfDigits) end, 0, DigitList),
    Number == ResArmstrongCalc.

