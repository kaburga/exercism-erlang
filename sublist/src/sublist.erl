-module(sublist).

-export([is_equal/2, is_sublist/2, is_superlist/2, is_unequal/2, relation/2]).

is_equal([], []) -> true;
is_equal([H1|T1], [H2|T2]) when H1 =:= H2 ->
    is_equal(T1, T2);
is_equal(_, _) -> false.

is_sublist([H|T1]=L1, [H|T2]) -> is_prefix(T1, T2) orelse is_sublist(L1, T2);
is_sublist([], _L2)  -> true;
is_sublist(_L1,  []) -> false;
is_sublist(L1, [_H2|T2]) -> is_sublist(L1, T2).

is_prefix([], []) -> true;
is_prefix([], _L2) -> true;
is_prefix(_L1, []) -> false;
is_prefix([H|T1], [H|T2]) -> is_prefix(T1, T2);
is_prefix(_L1, _L2) -> false.

is_superlist(L1, L2) -> is_sublist(L2, L1).

is_unequal(L1, L2) ->
    case is_equal(L1, L2) of
        true  -> false;
        false -> true
    end.

relation(L1, L2) ->
    Eq = is_equal(L1, L2),
    SubL = is_sublist(L1, L2),
    SupL = is_superlist(L1, L2),
    if
        Eq   -> equal;
        SubL -> sublist;
        SupL -> superlist;
        true -> unequal
    end.
