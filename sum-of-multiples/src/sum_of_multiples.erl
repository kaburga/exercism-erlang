-module(sum_of_multiples).

-export([sum/2]).

sum(Factors, Limit) ->
    lists:sum(
      lists:usort(
        lists:flatten(
          list_of_multiples(Factors, Limit)))).

list_of_multiples([], _Limit) ->
    [];
list_of_multiples([Factor|Fs], Limit) ->
    [multiples_of(Factor, 1, Limit)|list_of_multiples(Fs, Limit)].

multiples_of(0, _Multiple, _Limit) -> [0];
multiples_of(Factor, Multiple, Limit) when Factor*Multiple >= Limit -> [0];
multiples_of(Factor, Multiple, Limit) ->
    [Factor*Multiple|multiples_of(Factor, Multiple+1, Limit)].

