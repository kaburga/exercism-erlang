-module(rail_fence_cipher).

-export([decode/2, encode/2]).


decode(_Message, Rails) when Rails =< 1 ->
    undefined;
decode(Message, Rails) ->
    RailDict = split_message_into_rails(Message, Rails),
    decipher(RailDict,
             string:length(Message)).

split_message_into_rails(Message, Rails) ->
    RailsDict = find_rail_sizes(Message, Rails),
    divide_message(RailsDict, Message).

divide_message(Rails, Message) ->
    divide_message(Rails,
                   1,
                   dict:size(Rails),
                   Message).

divide_message(Dict, _N, _NumRails, []) ->
    Dict;
divide_message(Dict, N, NumRails, Message) ->
    SubMsgLen = dict:fetch(N, Dict),
    {M, RestMsg} = lists:split(SubMsgLen, Message),
    NewDict = dict:store(N, M, Dict),
    divide_message(NewDict,
                   N+1,
                   NumRails,
                   RestMsg).

find_rail_sizes(Message, Rails) ->
    Cycles = (Rails * 2) - 2,
    MessageLength = string:length(Message),
    RailSize = MessageLength div Cycles,
    SpareUnits = MessageLength rem Cycles,
    Dict = create_rails_dict(dict:new(),
                             Rails,
                             RailSize),
    distribute_spare_units(Dict, SpareUnits).

create_rails_dict(Dict, NumRails, RailSize) ->
    create_rails_dict(Dict, 1, NumRails, RailSize).

create_rails_dict(Dict, 1, NumRails, RailSize) ->
    NewDict = dict:store(1, RailSize, Dict),
        create_rails_dict(NewDict,
                          2,
                          NumRails,
                          RailSize);
create_rails_dict(Dict, NumRails, NumRails, RailSize) ->
    dict:store(NumRails, RailSize, Dict);
create_rails_dict(Dict, N, NumRails, RailSize) ->
    NewDict = dict:store(N, RailSize*2, Dict),
    create_rails_dict(NewDict,
                      N+1,
                      NumRails,
                      RailSize).

distribute_spare_units(Dict, SpareUnits) ->
    distribute_spare_units(Dict,
                           SpareUnits,
                           1,
                           dict:size(Dict)).

distribute_spare_units(Dict, 0, _N, _NumRails) ->
    Dict;
distribute_spare_units(Dict, Spares, NumRails, NumRails) ->
    NewDict = dict:update_counter(NumRails, 1, Dict),
    distribute_spare_units(NewDict,
                           Spares-1,
                           1,
                           NumRails);
distribute_spare_units(Dict, Spares, N, NumRails) ->
    NewDict = dict:update_counter(N, 1, Dict),
    distribute_spare_units(NewDict,
                           Spares-1,
                           N+1,
                           NumRails).

decipher(RailDict, Length) ->
    RailsList = lists:sort(dict:fetch_keys(RailDict)),
    decipher(Length,
             [],
             RailDict,
             RailsList,
             RailsList).

decipher(0, Message, _Dict, _Rs, _Rails) ->
    Message;
decipher(Length, Message, Dict, [_R|[]], Rails) ->
    decipher(Length,
             Message,
             Dict,
             lists:reverse(Rails),
             lists:reverse(Rails));
decipher(Length, Message, Dict, [R|Rs], Rails) ->
    [Ch|RMsg] = dict:fetch(R, Dict),
    NewDict = dict:store(R, RMsg, Dict),
    decipher(Length-1,
             Message++[Ch],
             NewDict,
             Rs,
             Rails).

encode(_Message, Rails) when Rails =< 1 ->
    undefined;
encode(Message, Rails) ->
    cipher(Message, Rails).

cipher(Message, NumRails) ->
    Rails = lists:seq(1, NumRails),
    cipher(dict:new(),
           Message,
           Rails,
           Rails).

cipher(Dict, [], _Rs, _Rails) ->
    RailStrings = lists:keysort(1, dict:to_list(Dict)),
    lists:foldl(fun({_K, RailString}, AccString) ->
                        AccString ++ RailString
                 end,
                 [],
                 RailStrings);
cipher(Dict, Message, [], Rails) when erlang:length(Rails) =:= 2 ->
    cipher(Dict,
           Message,
           Rails,
           Rails);
cipher(Dict, Message, [_R|[]], Rails) ->
    cipher(Dict,
           Message,
           lists:reverse(Rails),
           lists:reverse(Rails));
cipher(Dict, [Ch|Message], [R|Rs], Rails) ->
    NewDict = dict:append(R, Ch, Dict),
    cipher(NewDict, Message, Rs, Rails).

