-module(darts).

-export([score/2]).


score(0, 0) -> 10;
score(X, Y) when X >= 0 andalso X =< 5 andalso Y >= 0 andalso Y =< 5 -> 5;
score(X, Y) when X > 5 andalso X =< 10 andalso Y >= 0 andalso Y =< 10 -> 1;
score(X, Y) when X >= 0 andalso X =< 10 andalso Y > 5 andalso Y =< 10 -> 1;
score(_, _) -> 0.

