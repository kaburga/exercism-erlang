-module(forth).

-export([evaluate/1]).

evaluate(InstructionLists) ->
    UserDefInsTable = ets:new(user_def_ins, [set, {heir, none}]),
    [Result] = lists:filtermap(fun(InsList)->
                                       Res = evaluateInstructionList(InsList, UserDefInsTable),
                                       case Res of
                                           ok -> false;
                                           _ -> {true, Res}
                                       end
                               end, InstructionLists),
    ets:delete(UserDefInsTable),
    Result.

formatInstructionList(Instructions) ->
    lists:map(fun(Item)-> case string:to_integer(Item) of
                              {error, no_integer} -> string:casefold(Item);
                              {Int, _} -> Int
                          end
              end, string:lexemes(Instructions, " ")).

update_user_defined_instruction_table(Table, {InsName, InsDefinition}) ->
    ExpandedIns = lists:map(fun(Ins)->
                                    case ets:lookup(Table, Ins) of
                                        [{_, [Def]}] -> Def;
                                        [] -> Ins
                                    end
                            end, InsDefinition),
    ets:insert(Table, {string:casefold(InsName), ExpandedIns}).

evaluateInstructionList([], _UserDefInsTable) ->
    [];
evaluateInstructionList(":"++UserDefInstruction, UserDefInsTable) ->
    case re:run(UserDefInstruction, "\s(.*)\s(.*)\s;$", [{capture, all_but_first, list}, ungreedy]) of
        {match, [InsName, Definition]} ->
            InsDefinition = formatInstructionList(Definition),
            update_user_defined_instruction_table(UserDefInsTable, {InsName, InsDefinition}),
            ok;
        [nomatch] ->
            error("Bad syntax")
    end;

evaluateInstructionList(Instructions, UserDefInsTable) ->
    InstructionList = formatInstructionList(Instructions),
    evaluateInstructionList(InstructionList, [], UserDefInsTable).

evaluateInstructionList([], Stack, _UserDefInsTable) ->
    lists:reverse(Stack);
evaluateInstructionList([Instruction|Ins], Stack, UserDefInsTable) when erlang:is_integer(Instruction) ->
    evaluateInstructionList(Ins, [Instruction]++Stack, UserDefInsTable);

evaluateInstructionList([Instruction|InsList], Stack, UserDefInsTable) ->
    case ets:lookup(UserDefInsTable, Instruction) of
        [{Instruction, Def}] ->
            evaluateInstructionList(Def++InsList, Stack, UserDefInsTable);
        [] ->
            case Instruction of
                "+" ->
                    {[A,B], Rs} = lists:split(2, Stack),
                    Result = A + B,
                    evaluateInstructionList(InsList, [Result]++Rs, UserDefInsTable);
                "-" ->
                    {[A,B], Rs} = lists:split(2, Stack),
                    Result = B - A,
                    evaluateInstructionList(InsList, [Result]++Rs, UserDefInsTable);
                "*" ->
                    {[A,B], Rs} = lists:split(2, Stack),
                    Result = A * B,
                    evaluateInstructionList(InsList, [Result]++Rs, UserDefInsTable);
                "/" ->
                    {[A,B], Rs} = lists:split(2, Stack),
                    Result = B div A,
                    evaluateInstructionList(InsList, [Result]++Rs, UserDefInsTable);
                "dup" ->
                    {[A], Rs} = lists:split(1, Stack),
                    evaluateInstructionList(InsList, [A,A]++Rs, UserDefInsTable);
                "drop" ->
                    Rs = erlang:tl(Stack),
                    evaluateInstructionList(InsList, Rs, UserDefInsTable);
                "swap" ->
                    {[A,B], Rs} = lists:split(2, Stack),
                    evaluateInstructionList(InsList, [B,A]++Rs, UserDefInsTable);
                "over" ->
                    B = lists:nth(2, Stack),
                    evaluateInstructionList(InsList, [B]++Stack, UserDefInsTable);
                _ ->
                    io:format("~s~n", [Instruction]),
                    error("unknown instruction")
            end
    end.

