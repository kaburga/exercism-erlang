-module(nth_prime).

-export([prime/1]).

prime(0) -> error("");
prime(N) ->
    nth_prime(2, 1, N).

nth_prime(X, N, N) ->
    X;
nth_prime(X, M, N) ->
    case is_prime(X+1) of
        true  -> nth_prime(X+1, M+1, N);
        false -> nth_prime(X+1, M, N)
    end.

is_prime(2) -> true;
is_prime(3) -> true;
is_prime(5) -> true;
is_prime(7) -> true;
is_prime(11) -> true;
is_prime(N) when N rem 2 == 0 -> false;
is_prime(N) when is_integer(N) andalso N > 12 ->
    Ceil = erlang:trunc(math:sqrt(N)),
    Ms = lists:seq(3, Ceil),
    is_prime(N, Ms);
is_prime(_N) -> false.

is_prime(_N, []) -> true;
is_prime(N, [M|T]) ->
    case N rem M == 0 of
        true  -> false;
        false -> is_prime(N, T)
    end.

