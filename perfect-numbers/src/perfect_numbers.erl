-module(perfect_numbers).

-export([classify/1]).


classify(Number) when Number > 0 -> 
    Sum = factor_sum(Number, 0),
    if
        Sum == Number -> perfect;
        Sum > Number  -> abundant;
        Sum < Number  -> deficient
    end.

factor_sum(Number, Sum) ->
    factor_sum(1, Number, Sum).

factor_sum(Number, Number, Sum) ->
    Sum;
factor_sum(N, Number, Sum) ->
    if
        (Number rem N) == 0 -> factor_sum(N+1, Number, Sum+N);
        true -> factor_sum(N+1, Number, Sum)
    end.

