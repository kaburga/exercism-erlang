-module(circular_buffer).
-behaviour(gen_server).

-export([create/1, read/1, size/1, write/2, write_attempt/2]).
-export([init/1, handle_call/3, handle_cast/2]).

-record(buffer, {data = none,
                 read_pos = none,
                 write_pos = none,
                 current_num_elems = none,
                 max_size = none}).

create(Size) ->
    case gen_server:start_link(?MODULE, Size, []) of
        {ok, Pid} -> Pid;
        Err -> Err
    end.

read(Pid) ->
    gen_server:call(Pid, read).

size(Pid) ->
    {ok, gen_server:call(Pid, size)}.

write(Pid, Item) ->
    gen_server:cast(Pid, {write, Item}).

write_attempt(Pid, Item) ->
    gen_server:call(Pid, {write_attempt, Item}).

init(Size) ->
    Index = lists:seq(1, Size),
    InitValues = lists:duplicate(Size, empty),
    Data = lists:zip(Index, InitValues),
    Buffer = #buffer{data=Data,
                     read_pos=1,
                     write_pos=1,
                     current_num_elems=0,
                     max_size=Size},
    {ok, Buffer}.

handle_call(read, _From, Buffer) when Buffer#buffer.current_num_elems == 0 ->
    {reply, {error, empty}, Buffer};
handle_call(read, _From, Buffer) ->
    {NewBuffer, Item} = read_from_buffer(Buffer),
    {reply, {ok, Item}, NewBuffer};

handle_call(size, _From, Buffer) ->
    {reply, Buffer#buffer.max_size, Buffer};


handle_call({write_attempt, _Item}, _From, Buffer) when Buffer#buffer.current_num_elems == Buffer#buffer.max_size ->
    {reply, {error, full}, Buffer};

handle_call({write_attempt, Item}, _From, Buffer) ->
    NewBuffer = write_to_buffer(Buffer, Item),
    {reply, ok, NewBuffer}.

handle_cast({write, Item}, Buffer) ->
    NewBuffer = write_to_buffer(Buffer, Item),
    {noreply, NewBuffer}.


read_from_buffer(Buffer) ->
    {_Key, Item} = lists:keyfind(Buffer#buffer.read_pos,
                                 1,
                                 Buffer#buffer.data),
    NewReadPos = determine_new_position(Buffer#buffer.read_pos, Buffer#buffer.max_size),
    NewNumElems = Buffer#buffer.current_num_elems - 1,
    NewBuffer = Buffer#buffer{read_pos = NewReadPos,
                              current_num_elems = NewNumElems},
    {NewBuffer, Item}.

write_to_buffer(Buffer, Item) ->
    NewData = lists:keyreplace(Buffer#buffer.write_pos,
                               1,
                               Buffer#buffer.data,
                               {Buffer#buffer.write_pos, Item}),
    NewWritePos = determine_new_position(Buffer#buffer.write_pos, Buffer#buffer.max_size),
    {NewReadPos, NewNumElems} = case Buffer#buffer.current_num_elems == Buffer#buffer.max_size of
                                    true  -> {determine_new_position(Buffer#buffer.read_pos,
                                                                     Buffer#buffer.max_size),
                                              Buffer#buffer.current_num_elems};
                                    false -> {Buffer#buffer.read_pos,
                                              Buffer#buffer.current_num_elems + 1}
                 end,
    Buffer#buffer{data = NewData,
                  write_pos = NewWritePos,
                  read_pos = NewReadPos,
                  current_num_elems = NewNumElems}.

determine_new_position(Pos, Max) ->
    if
        Pos == (Max - 1) -> Max;
        Pos == Max -> 1;
        true -> (Pos + 1)
    end.
