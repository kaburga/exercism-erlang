-module(raindrops).

-export([convert/1]).

convert(Number) ->
    Output = [raindrop(X) || X <- [3,5,7], Number rem X == 0],
    convert(Number, Output).

convert(Number, []) ->
    erlang:integer_to_list(Number);
convert(_Number, Output) ->
    lists:flatten(Output).

raindrop(3) ->
    "Pling";
raindrop(5) ->
    "Plang";
raindrop(7) ->
    "Plong".
