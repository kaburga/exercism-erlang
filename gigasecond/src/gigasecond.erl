-module(gigasecond).

-export([from/1]).

from(From) ->
    case From of
        {_, _} ->
            Datetime = From;
        {_, _, _} ->
            Datetime = {From, {0, 0, 0}}
    end,
    Secs = calendar:datetime_to_gregorian_seconds(Datetime) + 1000000000,
    calendar:gregorian_seconds_to_datetime(Secs).
