-module(robot_simulator).
-behaviour(gen_server).

-export([advance/1, create/0, direction/1, left/1, place/3, position/1, right/1]).
-export([init/1, handle_call/3, handle_cast/2]).

create() ->
    case gen_server:start_link(?MODULE, [], []) of
        {ok, Pid} -> Pid;
        _ -> error
    end.

place(Robot, Direction, Position) ->
    gen_server:cast(Robot, {Direction, Position}).

position(Robot) ->
    gen_server:call(Robot, position).

direction(Robot) ->
    gen_server:call(Robot, direction).

advance(Robot) ->
    gen_server:cast(Robot, advance).

left(Robot) ->
    gen_server:cast(Robot, turn_left).

right(Robot) ->
    gen_server:cast(Robot, turn_right).

init(_Args) ->
    {ok, {undefined, {0, 0}}}.

handle_cast(advance, State) ->
    {Direction, {X, Y}} = State,
    NewPosition = case Direction of
                      north -> {X, Y+1};
                      south -> {X, Y-1};
                      east  -> {X+1, Y};
                      west  -> {X-1, Y}
                      end,
    {noreply, {Direction, NewPosition}};

handle_cast(turn_right, State) ->
    {Direction, Position} = State,
    NewDirection = case Direction of
                       north -> east;
                       east  -> south;
                       south -> west;
                       west  -> north
                   end,
    {noreply, {NewDirection, Position}};

handle_cast(turn_left, State) ->
    {Direction, Position} = State,
    NewDirection = case Direction of
                       north -> west;
                       east  -> north;
                       south -> east;
                       west  -> south
                   end,
    {noreply, {NewDirection, Position}};

handle_cast({Direction, {X, Y}}, _State) ->
    {noreply, {Direction, {X, Y}}}.

handle_call(direction, _From, State) ->
    {Direction, _Position} = State,
    {reply, Direction, State};

handle_call(position, _From, State) ->
    {_Direction, Position} = State,
    {reply, Position, State}.
