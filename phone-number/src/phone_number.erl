-module(phone_number).

-export([number/1, areacode/1, pretty_print/1]).

number(String) ->
    Number = re:replace(String, "-| |\\.|\\(|\\)", "", [global, {return,list}]),
    if
        erlang:length(Number) == 10 -> Number;
        erlang:length(Number) == 11 andalso erlang:hd(Number) == $1 -> string:slice(Number, 1);
        true -> "0000000000"
    end.

areacode(String) ->
    string:slice(String, 0, 3).

pretty_print(String) ->
    PhoneNum = number(String),
    AreaCode = areacode(PhoneNum),
    Part1 = string:slice(PhoneNum, 3, 3),
    Part2 = string:slice(PhoneNum, 6),
    lists:flatten(
      io_lib:format("(~s) ~s-~s", [AreaCode, Part1, Part2])).

