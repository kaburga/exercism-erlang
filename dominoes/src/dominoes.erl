-module(dominoes).

-export([can_chain/1]).

can_chain([]) -> true;
can_chain([{Side1, Side2}]) -> Side1 =:= Side2;
can_chain([{Side1, _}=D|Dominoes]) ->
    lists:any(fun(Match)->
                      can_chain([reduce(D, Match) | Dominoes -- [Match]])
              end,
              [Match || {S1, S2} = Match <- Dominoes,
                        S1 == Side1 orelse S2 == Side1]).

reduce({Match, Right1}, {Left2, Match}) ->
    {Left2, Right1};
reduce({Match, Right1}, {Match, Right2}) ->
    {Right2, Right1}.
