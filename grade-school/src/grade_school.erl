-module(grade_school).

-export([add/3, get/2, get/1, new/0]).


add(Name, Grade, School) ->
    dict:append(Grade, Name, School).

get(Grade, School) ->
    case dict:find(Grade, School) of
        error -> [];
        {ok, Values} -> Values
    end.

get(School) ->
    dict:fold(fun(_Key, Value, Acc)-> Acc ++ Value end, [], School).

new() ->
    dict:new().
