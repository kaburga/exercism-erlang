-module(meetup).

-export([meetup/4]).


meetup(Year, Month, DayOfWeek, Week) ->
    WeekDayNum = day_of_week_num(DayOfWeek),
    DayInMonthNum = date_of_first_day_in_week(Week, Year, Month),
    FirstDayOfWeekNum = calendar:day_of_the_week(Year, Month, DayInMonthNum),
    MeetupDay = DayInMonthNum + determine_meetup_day_in_month(WeekDayNum, FirstDayOfWeekNum),
    {Year, Month, MeetupDay}.

determine_meetup_day_in_month(Day, FirstInWeek) ->
    modulo(Day - FirstInWeek, 7).

modulo(X,Y) when X > 0 ->
    X rem Y;
modulo(X,Y) when X < 0 ->
    K = (-X div Y) + 1,
    PositiveX = X + (K*Y),
    PositiveX rem Y;
modulo(0,_Y) ->
    0.

date_of_first_day_in_week(first, _Year, _Month)  ->  1;
date_of_first_day_in_week(second, _Year, _Month) ->  8;
date_of_first_day_in_week(teenth, _Year, _Month) -> 13;
date_of_first_day_in_week(third, _Year, _Month)  -> 15;
date_of_first_day_in_week(fourth, _Year, _Month) -> 22;
date_of_first_day_in_week(last, Year, Month)     -> calendar:last_day_of_the_month(Year, Month) - 6.

day_of_week_num(monday)   -> 1;
day_of_week_num(tuesday)  -> 2;
day_of_week_num(wednesday)-> 3;
day_of_week_num(thursday) -> 4;
day_of_week_num(friday)   -> 5;
day_of_week_num(saturday) -> 6;
day_of_week_num(sunday)   -> 7.
