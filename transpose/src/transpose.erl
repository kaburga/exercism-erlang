-module(transpose).

-export([transpose/1]).

transpose([]) -> [];
transpose([Line|[]]) ->
    split_line(Line);
transpose(Lines) ->
    PLines = pad_lines(Lines),
    transpose(PLines, []).

split_line([]) -> [];
split_line([H|T]) ->
    [[H]]++split_line(T).


pad_lines(Lines) ->
    Max = max_line_len(Lines),
    pad_lines(Lines, Max).

pad_lines(Lines, Max) ->
    InsertPadding = fun(L) ->
                            LLen = erlang:length(L),
                            case LLen < Max of
                                true  -> L ++ lists:duplicate(Max-LLen, $ );
                                false -> L
                            end
                    end,
    lists:map(InsertPadding, Lines).

max_line_len(Lines) ->
    DetermineMax = fun(L, Max) ->
                           LLen = erlang:length(L),
                           case LLen > Max of
                               true  -> LLen;
                               false -> Max
                           end
                   end,
    lists:foldl(DetermineMax, 0, Lines).

transpose([[]|_T], Result) ->
    lists:reverse(Result);
transpose(Lines, Result) ->
    CleanedLines = cleanup_blank_lines(Lines),
    HList = [erlang:hd(Line) || Line <- CleanedLines],
    TList = [lists:nthtail(1, Line) || Line <- CleanedLines],
    transpose(TList, [HList]++Result).

cleanup_blank_lines(Lines) ->
    [LastLine|Rest] = lists:reverse(Lines),
    case string:trim(LastLine) of
        [] -> cleanup_blank_lines(
                lists:reverse(Rest));
        _  -> Lines
    end.
