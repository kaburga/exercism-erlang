-module(protein_translation).

-export([proteins/1]).

proteins(Strand) ->
    codon_to_protein(Strand).

codon_to_protein([X,Y,Z|_Cs]) when [X,Y,Z] =:= "UAA" orelse [X,Y,Z] =:= "UAG" orelse [X,Y,Z] =:= "UGA" ->
    [];
codon_to_protein([X,Y,Z|Cs]) ->
    Codon = [X,Y,Z],
    Protein = case Codon of
                  "AUG" -> [methionine];
                  "UUU" -> [phenylalanine];
                  "UUC" -> [phenylalanine];
                  "UUA" -> [leucine];
                  "UUG" -> [leucine];
                  "UCU" -> [serine];
                  "UCC" -> [serine];
                  "UCA" -> [serine];
                  "UCG" -> [serine];
                  "UAU" -> [tyrosine];
                  "UAC" -> [tyrosine];
                  "UGU" -> [cysteine];
                  "UGC" -> [cysteine];
                  "UGG" -> [tryptophan]
              end,
    Protein ++ codon_to_protein(Cs);
codon_to_protein([]) ->
    [];
codon_to_protein(_) ->
    error("Non-valid sequence").

