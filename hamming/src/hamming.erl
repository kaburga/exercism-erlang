-module(hamming).

-export([distance/2]).


distance(Strand1, Strand2) -> 
    case erlang:length(Strand1) == erlang:length(Strand2) of
        true  -> distance(Strand1, Strand2, 0);
        false -> {error,
                  "left and right strands must be of equal "
                  "length"}
    end.

distance([H1|Strand1], [H2|Strand2], Count) ->
    case H1 == H2 of
        true  -> distance(Strand1, Strand2, Count);
        false -> distance(Strand1, Strand2, Count+1)
    end;
distance([], [], Count) ->
    Count.

