-module(beer_song).

-export([verse/1, sing/1, sing/2]).

verse(N) when N < 100 andalso N > 2 ->
    NString = erlang:integer_to_list(N),
    NStringSub1 = erlang:integer_to_list(N-1),
    Line1 = unicode:characters_to_list([NString ," bottles of beer on the wall, ", NString, " bottles of beer.\n"], unicode),
    Line2 = unicode:characters_to_list(["Take one down and pass it around, ", NStringSub1, " bottles of beer on the wall.\n"], unicode),
    [Line1, Line2];
verse(2) ->
    ["2 bottles of beer on the wall, 2 bottles of beer.\n",
     "Take one down and pass it around, 1 bottle of beer on the wall.\n"];
verse(1) ->
    ["1 bottle of beer on the wall, 1 bottle of beer.\n",
     "Take it down and pass it around, no more bottles of beer on the wall.\n"];
verse(0) ->
    ["No more bottles of beer on the wall, no more bottles of beer.\n",
     "Go to the store and buy some more, 99 bottles of beer on the wall.\n"].

sing(0) ->
    verse(0) ++ ["\n"];
sing(N) ->
    verse(N) ++ ["\n"] ++ sing(N-1).

sing(From, To) when From > To  andalso From > 0 ->
    verse(From) ++ ["\n"] ++ sing(From-1, To);
sing(To, To) ->
    verse(To) ++ ["\n"].

