-module(sieve).

-export([primes/1]).

primes(2) -> [2];
primes(Limit) when Limit > 2 ->
    Range = lists:seq(2, Limit),
    primes([], Range);
primes(_Limit) -> [].


primes(PrimeList, []) ->
    PrimeList;
primes(PrimeList, [Prime|Rest]) ->
    primes(PrimeList ++ [Prime],
           remove_nonprimes(Prime, Rest)).

remove_nonprimes(Prime, Range) ->
    lists:filter(fun(Elem) ->
                         Elem rem Prime /= 0
                 end, Range).
