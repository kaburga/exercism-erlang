-module(space_age).

-export([age/2]).


-define(Earth,   31557600).
-define(Mercury, (?Earth * 0.2408467)).
-define(Venus,   (?Earth * 0.61519726)).
-define(Mars,    (?Earth * 1.8808158)).
-define(Jupiter, (?Earth * 11.862615)).
-define(Saturn,  (?Earth * 29.447498)).
-define(Uranus,  (?Earth * 84.016846)).
-define(Neptune, (?Earth * 164.79132)).

age(Planet, Seconds) ->
    Age = case Planet of
              earth    -> Seconds / ?Earth;
              mercury  -> Seconds / ?Mercury;
              venus    -> Seconds / ?Venus;
              mars     -> Seconds / ?Mars;
              jupiter  -> Seconds / ?Jupiter;
              saturn   -> Seconds / ?Saturn;
              uranus   -> Seconds / ?Uranus;
              neptune  -> Seconds / ?Neptune
          end,
    erlang:list_to_float(
      erlang:float_to_list(Age, [{decimals, 2}])).
