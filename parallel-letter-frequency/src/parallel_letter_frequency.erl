-module(parallel_letter_frequency).

-export([dict/1]).

dict(Strings) ->
    Parent = self(),
    ChildPids = [spawn_link(fun() -> Parent ! {self(), letter_frequency(S)} end)
            || S <- Strings],
    Dicts = [receive {Pid, R} -> R end || Pid <- ChildPids],
    sum_results(Dicts).

letter_frequency(String) ->
    Dict = dict:new(),
    lists:foldl(fun(Ch, Acc) ->
                        dict:update_counter(Ch, 1, Acc)
                end,
                Dict, String).

sum_results(Dicts) ->
    sum_results(Dicts, dict:new()).

sum_results([], SumDict) ->
    SumDict;
sum_results([Dict|Ds], SumDict) ->
    NewSumDict = dict:merge(fun(_Key, Val1, Val2) ->
                       Val1 + Val2
                            end, Dict, SumDict),
    sum_results(Ds, NewSumDict).

