-module(bracket_push).

-export([is_paired/1]).


is_paired([]) -> true;
is_paired(String) ->
    {Brackets, _} = lists:partition(fun(I)-> lists:member(I, "[](){}") end, String),
    bracket_matcher(Brackets, []).

bracket_matcher([], []) ->
    true;
bracket_matcher([], _Stack) ->
    false;
bracket_matcher("("++Brackets, Stack) ->
    bracket_matcher(Brackets, ["("|Stack]);
bracket_matcher("["++Brackets, Stack) ->
    bracket_matcher(Brackets, ["["|Stack]);
bracket_matcher("{"++Brackets, Stack) ->
    bracket_matcher(Brackets, ["{"|Stack]);
bracket_matcher(")"++Brackets, ["("|Stack])->
    bracket_matcher(Brackets, Stack);
bracket_matcher("]"++Brackets, ["["|Stack])->
    bracket_matcher(Brackets, Stack);
bracket_matcher("}"++Brackets, ["{"|Stack])->
    bracket_matcher(Brackets, Stack);
bracket_matcher(_Brackets, _Stack) ->
    false.
