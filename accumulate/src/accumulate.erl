-module(accumulate).

-export([accumulate/2]).

accumulate(Fn, Ls) ->
    accumulate(Fn, Ls, []).

accumulate(Fn, [H|T], Res) ->
    accumulate(Fn, T, [Fn(H)|Res]);
accumulate(_Fn, [], Res) ->
    lists:reverse(Res).

