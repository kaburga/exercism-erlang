-module(complex_numbers).

-export([abs/1, add/2, conjugate/1, divide/2, equal/2, exp/1, imaginary/1, mul/2, new/2,
	 real/1, sub/2]).


abs(Z) ->
    {R, I} = Z,
    math:sqrt(math:pow(R, 2) + math:pow(I, 2)).

add(Z1, Z2) ->
    {R1, I1} = Z1,
    {R2, I2} = Z2,
    R3 = (R1+R2),
    I3 = (I1+I2),
    {R3, I3}.

conjugate(Z) ->
    {R, I} = Z,
    {R, -I}.

divide(Z1, Z2) ->
    {R1, I1} = Z1,
    {R2, I2} = Z2,
    R3 = (R1*R2 + I1*I2) / (math:pow(R2,2) + math:pow(I2,2)),
    I3 = (I1*R2 - R1*I2) / (math:pow(R2,2) + math:pow(I2,2)),
    {R3, I3}.

equal(Z1, Z2) ->
    {R1, I1} = Z1,
    {R2, I2} = Z2,
    erlang:abs(R1 - R2) < 0.00001 andalso erlang:abs(I1 - I2) < 0.00001.

exp(Z) ->
    {R, I} = Z,
    R2 = math:exp(R) * math:cos(I),
    I2 = math:exp(R) * math:sin(I),
    {R2, I2}.

imaginary(Z) ->
    {_R, I} = Z,
    I.

mul(Z1, Z2) ->
    {R1, I1} = Z1,
    {R2, I2} = Z2,
    R3 = (R1*R2 - I1*I2),
    I3 = (I1*R2 + R1*I2),
    {R3, I3}.

new(R, I) ->
    {R, I}.

real(Z) ->
    {R, _I} = Z,
    R.

sub(Z1, Z2) ->
    {R1, I1} = Z1,
    {R2, I2} = Z2,
    R3 = (R1-R2),
    I3 = (I1-I2),
    {R3, I3}.
