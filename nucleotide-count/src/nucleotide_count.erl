-module(nucleotide_count).

-export([count/2, nucleotide_counts/1]).


count(Strand, [Nucleotide]) -> 
    count(Strand, Nucleotide, 0).


count([Head|Tail], Nucleotide, Count) ->
    
    Valid = lists:member(Head, [$A, $C, $G, $T]),
    if
        Valid == false -> error("Not a valid Nucleotide");
        Head == Nucleotide -> count(Tail, Nucleotide, Count+1);
        true -> count(Tail, Nucleotide, Count)
    end;
count([], _Nucleotide, Count) ->
    Count.


nucleotide_counts(Strand) -> 
    nucleotide_counts(Strand, {0, 0, 0, 0}).

nucleotide_counts([Head|Tail], {A, C, G, T}) ->    
    case Head of
        $A ->
            nucleotide_counts(Tail, {A+1, C, G, T});
        $C -> 
            nucleotide_counts(Tail, {A, C+1, G, T});
        $G -> 
            nucleotide_counts(Tail, {A, C, G+1, T});
        $T -> 
            nucleotide_counts(Tail, {A, C, G, T+1});
        _   ->
            error("Not a valid nucleotide")
    end;
nucleotide_counts([], {A, C, G, T}) ->
    lists:sort([{"A", A}, {"C", C}, {"G", G}, {"T", T}]).

