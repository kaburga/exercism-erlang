-module(etl).

-export([transform/1]).


transform(Old) ->
    New_Format_Map = maps:new(),
    maps:to_list(
      lists:foldl(fun transform/2, New_Format_Map, Old)).

transform({N, CharList}, Map) ->
    UpdateMap = fun(Ch, AccMap) ->
                        LowerCh = string:lowercase(Ch),
                        maps:put(LowerCh, N, AccMap)
                end,
    lists:foldl(UpdateMap, Map, CharList).
