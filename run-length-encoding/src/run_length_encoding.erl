-module(run_length_encoding).

-export([decode/1, encode/1]).

decode(Items) ->
    unicode:characters_to_list(unpack_sublist(Items)).

unpack_sublist([]) -> [];
unpack_sublist(Items) ->
    {MaybeInt, Rest} = string:to_integer(Items),
    case is_integer(MaybeInt) of
        false ->
            [hd(Items) | unpack_sublist(tl(Items))];
        true  ->
            [Hd|Tl]=Rest,
            [string:copies([Hd], MaybeInt) | unpack_sublist(Tl)]
    end.

encode([]) -> [];
encode([Hd|Tl]) ->
    lists:flatten(
      lists:map(fun(SubList)->
                        if
                            length(SubList) == 1 -> SubList;
                            true -> [integer_to_list(length(SubList)) | [hd(SubList)]]
                        end
                end,
                pack_sublist(Hd, [Hd], Tl))).

pack_sublist(_Ch, SubList, []) ->
    [SubList];
pack_sublist(Ch, SubList, [Ch|Rest]) ->
    pack_sublist(Ch, [Ch|SubList], Rest);
pack_sublist(_Ch, SubList, [Ch2|Rest]) ->
    [SubList | pack_sublist(Ch2, [Ch2], Rest)].
