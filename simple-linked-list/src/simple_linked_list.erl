-module(simple_linked_list).

-export([cons/2, count/1, empty/0, from_native_list/1,
         head/1, reverse/1, tail/1, to_native_list/1]).

-record(list, {head = none,
               tail = none,
               size = 0}).

-record(list_node, {item = none,
                    next = none}).

empty() ->
    #list{}.

cons(Elt, List) when List == #list{} ->
    NewNode = #list_node{item = Elt},
    List#list{head = NewNode,
              tail = NewNode,
              size = 1};
cons(Elt, List) ->
    NewNode = #list_node{item = Elt, next = List#list.head},
    NewSize = List#list.size + 1,
    List#list{head = NewNode, size = NewSize}.

head(List) ->
    Head = List#list.head,
    Head#list_node.item.

tail(List) ->
    Head = List#list.head,
    NewSize = List#list.size - 1,
    List#list{head = Head#list_node.next,
              size = NewSize}.

reverse(List) ->
    reverse(empty(), List).
reverse(RList, List) when List == #list{} ->
    RList;
reverse(RList, List) ->
    {Item, NewList} = pop(List),
    NewRList = cons(Item, RList),
    reverse(NewRList, NewList).

pop(List) when List#list.size == 0 ->
    {none, List};
pop(List) when List#list.size == 1 ->
    Node = List#list.head,
    NodeItem = Node#list_node.item,
    NewList = List#list{head = none, tail = none, size = 0},
    {NodeItem, NewList};
pop(List) ->
    Node = List#list.head,
    NodeItem = Node#list_node.item,
    NewSize = List#list.size - 1,
    NewList = List#list{head = Node#list_node.next, size = NewSize},
    {NodeItem, NewList}.

count(List) -> List#list.size.


to_native_list(List) ->
    to_native_list([], List).

to_native_list(NativeList, List) when List == #list{} ->
    NativeList;
to_native_list(NativeList, LList) ->
    {Item, NewLList} = pop(LList),
    to_native_list(NativeList++[Item], NewLList).


from_native_list(NativeList) ->
    from_native_list(lists:reverse(NativeList),
                     empty()).
from_native_list([], LList) ->
    LList;
from_native_list([H|T], LList) ->
    NewLList = cons(H, LList),
    from_native_list(T, NewLList).
