-module(series).

-export([slices/2]).

slices(_SliceLength, []) ->
    error("Invalid Series");
slices(SliceLength, Series) when SliceLength > erlang:length(Series) ->
    error("Invalid SliceLength");
slices(SliceLength, Series) when erlang:length(Series) =< SliceLength ->
    [Series];
slices(SliceLength, Series) ->
    [_H|T] = Series,
    [lists:sublist(Series, SliceLength) | slices(SliceLength, T)].

