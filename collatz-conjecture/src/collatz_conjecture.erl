-module(collatz_conjecture).

-export([steps/1]).

steps(N) when N  > 0 -> steps(N, 0);
steps(N) when N =< 0 -> {error, "Only positive numbers are allowed"}.

steps(1, Count) -> Count;
steps(N, Count) -> 
    case (N rem 2) of
        1 -> steps(N*3 + 1, Count+1); 
        0 -> steps(N div 2, Count+1)
    end.

