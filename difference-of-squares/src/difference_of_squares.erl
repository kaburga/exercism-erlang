-module(difference_of_squares).

-export([difference_of_squares/1, square_of_sum/1, sum_of_squares/1]).


difference_of_squares(Number) ->
    square_of_sum(Number) - sum_of_squares(Number).

square_of_sum(0) ->
    0;
square_of_sum(Number) ->
    square_of_sum(Number, 0).

square_of_sum(1, Sum) ->
    erlang:trunc(math:pow(Sum+1, 2));
square_of_sum(Number, Sum) ->
    square_of_sum(Number-1, Sum+Number).

sum_of_squares(0) ->
    0;
sum_of_squares(Number) ->
    sum_of_squares(Number, 0).

sum_of_squares(1, Sum) ->
    Sum+1;
sum_of_squares(Number, Sum) ->
    NewSum = Sum + erlang:trunc(math:pow(Number, 2)),
    sum_of_squares(Number-1, NewSum).

