-module(anagram).

-export([find_anagrams/2]).


find_anagrams(Subject, Candidates) ->
    LowerSub = string:lowercase(Subject),
    SortedSub = lists:sort(LowerSub),
    F = fun(Can) ->
                LowerCan = string:lowercase(Can),
                LowerSub /=  LowerCan andalso SortedSub == lists:sort(LowerCan)
        end,
    lists:filter(F, Candidates).

