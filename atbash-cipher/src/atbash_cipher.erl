-module(atbash_cipher).

-export([decode/1, encode/1]).

generate_cipher() ->
    Alphabet = "abcdefghijklmnopqrstuvwxyz",
    RAlphabet = lists:reverse(Alphabet),
    lists:zip(Alphabet, RAlphabet).

-define(Cipher, generate_cipher()).

crypto(Phrase, Action) ->
    {Key, Value} = case Action of
              decode -> {2, 1};
              encode -> {1, 2}
             end,
    lists:map(fun(Ch) ->
                      case lists:keyfind(Ch, Key, ?Cipher) of
                          false -> Ch;
                          Ciph -> erlang:element(Value, Ciph)
                      end
              end, Phrase).

prepare_input(Phrase) ->
    re:replace(string:lowercase(Phrase), " |\\.|\\,", "", [global, {return,list}]).

decode(Phrase) ->
    TrimPhrase = prepare_input(Phrase),
    crypto(TrimPhrase, decode).

encode(Phrase) ->
    TrimPhrase = prepare_input(Phrase),
    Encrypted = crypto(TrimPhrase, encode),
    insert_spaces(Encrypted).

insert_spaces(Phrase) when length(Phrase) > 5 ->
    {Part, Rest} = lists:split(5, Phrase),
    Part ++ " " ++ insert_spaces(Rest);
insert_spaces(Phrase) ->
    Phrase.
