-module(queen_attack).

-export([can_attack/2]).

can_attack({WR, WC}=WhiteQueen, {BR, BC}=BlackQueen) when WR >= 0 andalso WR =< 7 andalso
                                                          WC >= 0 andalso WC =< 7 andalso
                                                          BR >= 0 andalso BR =< 7 andalso
                                                          BC >= 0 andalso BC =< 7 ->
    try_attack(WhiteQueen, BlackQueen);
can_attack(_WhiteQueen, _BlackQueen) ->
    false.

try_attack({_WR, Column}, {_BR, Column}) ->
    true;
try_attack({Row, _WC}, {Row, _BC}) ->
    true;
try_attack({WR, WC}, {BR, BC}) ->
    erlang:abs(BR-WR) =:= erlang:abs(BC-WC);
try_attack(_WhiteQueen, _BlackQueen) ->
    false.

