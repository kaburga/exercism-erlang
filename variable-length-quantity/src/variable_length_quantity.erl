-module(variable_length_quantity).

-export([encode/1, decode/1]).

-define(VLQ_Byte, 7).
-define(Bit_7, 128).

encode(Integers) ->
    lists:flatten(
      lists:map(fun(Integer)-> encode_integer(Integer) end,
                Integers)).

encode_integer(Integer) ->
    encode_integer(Integer, []).

encode_integer(Integer, []) when Integer < 128 ->
    [Integer];
encode_integer(0, Accumulator) ->
    Accumulator;
encode_integer(Integer, []) ->
    encode_integer(Integer bsr ?VLQ_Byte, [Integer rem ?Bit_7]);
encode_integer(Integer, Accumulator) ->
    encode_integer(Integer bsr ?VLQ_Byte, [(Integer rem ?Bit_7) bxor ?Bit_7 | Accumulator]).

decode(Integers) ->
    IntegerLists = partition_integer_list(Integers),
    Result = lists:flatten([decode_integers(L) || L <- IntegerLists]),
    case lists:member(undefined, Result) of
        true  -> undefined;
        false -> Result
    end.

decode_integers(Integers) ->
    decode_integers(Integers, 0).

decode_integers(Integer, _Accumulator) when Integer >= ?Bit_7 andalso erlang:is_list(Integer) == false ->
    undefined;
decode_integers([Integer], Accumulator) ->
    Integer+Accumulator;
decode_integers([Integer|IntegerList], Accumulator) ->
    NewAccumulatorValue = ((Integer-?Bit_7)+Accumulator) bsl ?VLQ_Byte,
    decode_integers(IntegerList, NewAccumulatorValue).

partition_integer_list(Integers) ->
    partition_integer_list(Integers, []).

partition_integer_list([], Accumulator) ->
    Accumulator;
partition_integer_list([Integer|IntegerList], Accumulator) ->
    IsBit7Set = fun(Int) -> Int >= ?Bit_7 end,
    case IsBit7Set(Integer) of
        true  -> partition_integer_list(IntegerList, [Integer|Accumulator]);
        false -> NextSublist = partition_integer_list(IntegerList, []),
                 SubList = lists:reverse([Integer|Accumulator]),
                 [SubList|NextSublist]
    end.
