-module(saddle_points).

-export([saddle_points/1]).

get_columns([[]|_T]) ->
    [];
get_columns(Matrix) ->
    {Column, RestMatrix} = lists:foldl(fun([Item|Row], {Column, RestMatrix}) ->
                                               {[Item|Column],
                                                [Row|RestMatrix]}
                                       end,
                                       {[], []}, Matrix),
    [Column|get_columns(RestMatrix)].

saddle_points(Matrix) ->
    Columns = get_columns(Matrix),
    saddle_points([], 0, Matrix, Columns).

saddle_points(SaddlePoints, _Y, [], _Columns) ->
    SaddlePoints;
saddle_points(SaddlePoints, Y, [Row|Rows], Columns) ->
    SPoint = check_for_saddle_point([], {Y, 0}, Row, Row, Columns),
    case SPoint of
        none ->
            saddle_points(SaddlePoints, Y+1, Rows, Columns);
        _ ->
            NewSaddlePoints = SPoint ++ SaddlePoints,
            saddle_points(NewSaddlePoints, Y+1, Rows, Columns)
    end.

check_for_saddle_point([], {_Y, _X}, [], _Row, []) ->
    none;
check_for_saddle_point(SaddlePoints, {_Y, _X}, [], _Row, []) ->
    SaddlePoints;
check_for_saddle_point(SaddlePoints, {Y, X}, [Item|Items], Row, [Column|Columns]) ->
    GrInRow = greater_or_equal_in_row(Item, Row),
    LowInCol = lesser_or_equal_in_column(Item, Column),
    case LowInCol andalso GrInRow of
        true  ->
            NewSaddlePoints = [{Y, X}|SaddlePoints],
            check_for_saddle_point(NewSaddlePoints,
                                   {Y, X+1},
                                   Items,
                                   Row,
                                   Columns);
        false ->
            check_for_saddle_point(SaddlePoints,
                                   {Y, X+1},
                                   Items,
                                   Row,
                                   Columns)
    end.

greater_or_equal_in_row(Item, Row) ->
    Item >= lists:max(Row).

lesser_or_equal_in_column(Item, Column) ->
    Item =< lists:min(Column).

