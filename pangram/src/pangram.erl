-module(pangram).

-export([is_pangram/1]).

-define(Alphabet, "ABCDEFGHIJKLMNOPQRSTUVWXYZ").

is_pangram([]) -> false;
is_pangram(Phrase) ->
    Trimmed = lists:usort(
                re:replace( string:uppercase(Phrase), "_|-| |\\.|\"|[0-9]", "", [global, {return, list}])),
    ?Alphabet == Trimmed.

