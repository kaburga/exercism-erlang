-module(list_ops).

-export([append/2, concat/1, filter/2, length/1, map/2, foldl/3, foldr/3,
         reverse/1]).

append([H|T], List3) ->
    [H|append(T, List3)];
append([], List3) ->
    List3.

concat([]) ->
    [];
concat([H|T]) ->
    append(H, concat(T)).

filter(_Function, []) ->
    [];
filter(Function, [H|T]) ->
    case Function(H) of
        true  -> [H|filter(Function, T)];
        false -> filter(Function, T)
    end.

length(List) ->
    length(List, 0).

length([], Count) ->
    Count;
length([_H|T], Count) ->
    length(T, Count+1).

map(_Function, []) ->
    [];
map(Function, [H|T]) ->
    [Function(H)|map(Function, T)].

foldl(_Function, Acc, []) ->
    Acc;
foldl(Function, Acc, [H|T]) ->
    foldl(Function, Function(H, Acc), T).

foldr(_Function, Acc, []) ->
    Acc;
foldr(Function, Acc, [H|T]) ->
    Function(H, foldr(Function, Acc, T)).

reverse(List) ->
    reverse(List, []).

reverse([], Res) ->
    Res;
reverse([H|T], Res) ->
    reverse(T, [H|Res]).
