-module(triangle).

-export([kind/3]).

kind(A, B, C) when A =< 0 orelse
                   B =< 0 orelse
                   C =< 0 ->
    {error, "all side lengths must be positive"};
kind(A, B, C) when A + B =< C orelse
                   B + C =< A orelse
                   A + C =< B ->
    {error, "side lengths violate triangle inequality"};
kind(_A, _A, _A) ->
    equilateral;
kind(_A, _A, _B) ->
    isosceles;
kind(_A, _B, _A) ->
    isosceles;
kind(_B, _A, _A) ->
    isosceles;
kind(_A, _B, _C) ->
    scalene.

