-module(pythagorean_triplet).

-export([triplets_with_sum/1]).

%% triplets_with_sum(N) ->
%%     [ {A,B,C} ||
%%         A <- lists:seq(1,N),
%%         B <- lists:seq(1,N),
%%         C <- lists:seq(1,N),
%%         A+B+C == N,
%%         A*A+B*B == C*C,
%%         A < B, B < C
%%     ].

triplets_with_sum(Limit) ->
    [{A, B, Limit - A - B}
     || A <- lists:seq(1, Limit div 3),
        B <- lists:seq(A + 1, Limit div 2),
        A * A + B * B =:= (Limit - A - B) * (Limit - A - B)].
