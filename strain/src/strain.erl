-module(strain).

-export([keep/2, discard/2]).

keep(Fn, List) ->
  keep(Fn, List, []).

keep(_Fn, [], Res) ->
    lists:reverse(Res);
keep(Fn, [H|T], Res) ->
    case Fn(H) of
        true  -> keep(Fn, T, [H|Res]);
        false -> keep(Fn, T, Res)
    end.

discard(Fn, List) ->
    discard(Fn, List, []).

discard(_Fn, [], Res) ->
    lists:reverse(Res);
discard(Fn, [H|T], Res) ->
    case Fn(H) of
        true  -> discard(Fn, T, Res);
        false -> discard(Fn, T, [H|Res])
    end.

