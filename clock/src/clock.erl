-module(clock).

-export([create/2, is_equal/2, minutes_add/2, to_string/1]).


create(Hour, Minute) ->
    Sum = sum_in_mins(Hour, Minute),
    H24 = Sum div 60,
    M60 = Sum rem 60,
    {H24, M60}.

sum_in_mins(Hour, Minute) ->
    Sum = (Hour*60 + Minute) rem (24*60),
    if
        Sum < 0 -> SumAdjusted = (24*60) + Sum;
        true    -> SumAdjusted = Sum
    end,
    SumAdjusted.

is_equal(Clock1, Clock2) ->
    {H1, M1} = Clock1,
    {H2, M2} = Clock2,
    (H1 == H2) and (M1 == M2).

minutes_add(Clock, Minutes) ->
    {H, M} = Clock,
    create(H, M+Minutes).

to_string(Clock) ->
    {H, M} = Clock,
    if
        H < 10 -> HStr = "0" ++ erlang:integer_to_list(H);
        true   -> HStr = erlang:integer_to_list(H)
    end,
    if
        M < 10 -> MStr = "0" ++ erlang:integer_to_list(M);
        true   -> MStr = erlang:integer_to_list(M)
    end,
    HStr ++ ":" ++ MStr.

