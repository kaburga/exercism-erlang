-module(prime_factors).

-export([factors/1]).


factors(Value) when Value >= 0 -> 
    factors(Value, 2, []).

factors(1, _I, Factors) ->    
    lists:sort(
      lists:reverse(Factors));
factors(Value, I, Factors) ->
    case (Value rem I) == 0 of
        true  -> factors(Value div I, I, [I] ++ Factors);
        false -> factors(Value, I+1, Factors)
    end.


