-module(bob).

-export([response/1]).


response(String) ->
    StrippedString = string:trim(String),
    Q = question(StrippedString),
    Y = yell(StrippedString),
    S = silence(StrippedString),
    if
        Q andalso Y -> "Calm down, I know what I'm doing!";
        S -> "Fine. Be that way!";
        Q -> "Sure.";
        Y -> "Whoa, chill out!";
        true -> "Whatever."
    end.

silence(String) ->
    String == "".

question("") ->
    false;
question(String) ->
    lists:last(String) == $?.

yell("") ->
    false;
yell(String) ->
    (string:uppercase(String) == String) and not (string:lowercase(String) == String).
