-module(poker).

-export([best_hands/1]).

best_hands([Hand|[]]) ->
    [Hand];
best_hands(Hands) ->
    HandsTupleList = convert_to_tuple_format(Hands),
    BestHand = determine_best(HandsTupleList),
    convert_to_output_format(BestHand).

convert_to_tuple_format(Hands) ->
    lists:map(fun(H)->
                      Cards = string:lexemes(H, " "),
                      lists:map(fun card_string_to_tuple/1, Cards)
              end, Hands).

card_string_to_tuple(Card) ->
    ConvertToInt = fun(C) -> {Rank, Suite} = lists:split(1, C),
                             case Rank of
                                 "J" -> {11, Suite};
                                 "Q" -> {12, Suite};
                                 "K" -> {13, Suite};
                                 "A" -> {14, Suite}
                             end
                   end,
    CardTuple = string:to_integer(Card),
    case CardTuple of
        {error, no_integer} -> ConvertToInt(Card);
        _ -> CardTuple
    end.

convert_to_output_format([Hand]) ->
    HandString = string:join(
                   lists:map(fun convert_to_string/1, Hand), " "),
    [HandString];
convert_to_output_format(Hands) ->
    lists:map(fun(H) ->
                      string:join(
                        lists:map( fun convert_to_string/1, H), " ")
              end, Hands).

convert_to_string({R,S}) ->
    RString = case R of
                  11 -> "J";
                  12 -> "Q";
                  13 -> "K";
                  14 -> "A";
                  1 -> "A";
                  _ -> erlang:integer_to_list(R)
              end,
    unicode:characters_to_list([RString, S], unicode).

determine_best(Hands) ->
    Funs = [{straight_flush, fun straight_flush/3}, {four_of_a_kind, fun four_of_a_kind/3},
            {full_house, fun full_house/3}, {flush, fun flush/3}, {straight, fun straight/3},
            {three_of_a_kind, fun three_of_a_kind/3}, {two_pair, fun two_pair/3}, {one_pair, fun one_pair/3}],
    determine_best(Hands, Funs).

determine_best(Hands, []) ->
    find_highest(Hands);
determine_best(Hands, [{FName, F}|Funs]) ->
    ValidHands = lists:filtermap(
                   fun(H)-> {Ranks, Suites} = split_cards(H),
                            F(Ranks, Suites, H) end,
                   Hands),
    if
        ValidHands == [] -> determine_best(Hands, Funs);
        true -> decide_winner_among_valid_hands(ValidHands, FName)
    end.

decide_winner_among_valid_hands([{_, Hand}], full_house) -> [Hand];
decide_winner_among_valid_hands([Hand], _) -> [Hand];
decide_winner_among_valid_hands(Hands, full_house) ->
    lists:map(
      fun({_, H}) -> H end,
      lists:foldl(fun(Hand, Acc) -> compare_hand_ranks(Hand, Acc) end,
                  [], Hands));
decide_winner_among_valid_hands(Hands, _) ->
    Decider = fun(Hand, Acc) ->
                      NonUniqueCards = card_ranks(Hand -- lists:ukeysort(1, Hand)),
                      compare_hand_ranks({NonUniqueCards, Hand}, Acc)
              end,
    Kicker = fun(Hand, Acc) ->
                     DuplicatesRemoved = card_ranks(lists:ukeysort(1, Hand)),
                     compare_hand_ranks({DuplicatesRemoved, Hand}, Acc)
             end,
    ValidHands = lists:map(fun({_, H}) -> H end,
                           lists:foldl(Decider, [], Hands)),
    if
        erlang:length(ValidHands) == 1 ->
            ValidHands;
        true ->
            lists:map(fun({_, H}) -> H end,
                      lists:foldl(Kicker, [], Hands))
    end.

compare_hand_ranks(H1, []) -> [H1];
compare_hand_ranks(H1, [H2|_]=Acc) ->
    {R1, _}=H1,
    {R2, _}=H2,
    if
        R1 > R2  -> [H1];
        R1 == R2 -> [H1]++Acc;
        true -> Acc
    end.

split_cards(Hand) ->
    SortedHand = lists:sort(
                   fun({R1, _}, {R2, _}) -> R1>R2 end,
                   Hand),
    Ranks = lists:reverse(get_ranks(SortedHand)),
    Suites = lists:reverse(get_suites(SortedHand)),
    {Ranks, Suites}.

card_ranks(Hand) ->
    lists:sort(
      fun(R1, R2) -> R1>R2 end,
      get_ranks(Hand)).

get_ranks(Hand) ->
    lists:foldl(fun({R, _}, Acc)-> [R]++Acc end, [], Hand).

get_suites(Hand) ->
    lists:foldl(fun({_, S}, Acc)-> [S]++Acc end, [], Hand).

find_highest(Hands) ->
    HighRankAndHandTuple = lists:map(fun(H)-> {card_ranks(H), H} end, Hands),
    lists:map(fun({_, H}) -> H end,
              lists:foldl(fun compare_hand_ranks/2, [], HighRankAndHandTuple)).

one_pair([A, A, _X, _Y, _Z], _, _) -> true;
one_pair([_X, A, A, _Y, _Z], _, _) -> true;
one_pair([_X, _Y, A, A, _Z], _, _) -> true;
one_pair([_X, _Y, _Z, A, A], _, _) -> true;
one_pair(_, _, _) -> false.

two_pair([A, A, B, B, _X], _, _) -> true;
two_pair([A, A, _X, B, B], _, _) -> true;
two_pair([_X, A, A, B, B], _, _) -> true;
two_pair(_, _, _) -> false.

three_of_a_kind([A, A, A, _X, _Y], _, _) -> true;
three_of_a_kind([_X, A, A, A, _X], _, _) -> true;
three_of_a_kind([_X, _Y, A, A, A], _, _) -> true;
three_of_a_kind(_, _, _) -> false.

straight([14, 5, 4, 3, 2], [S, _, _, _, _], Hand) -> {true, lists:keyreplace(14, 1, Hand, {1, S})};
straight([E, D, C, B, A], _, _) when A+1==B, B+1==C, C+1==D, D+1==E -> true;
straight(_, _, _) -> false.

flush(_, [S, S, S, S, S], _) -> true;
flush(_, _, _) -> false.

full_house([A, A, A, B, B]=Ranks, _, Hand) -> {true, {Ranks, Hand}};
full_house([A, A, B, B, B], _, Hand) -> {true, {[B, B, B, A, A], Hand}};
full_house(_, _, _) -> false.

four_of_a_kind([A, A, A, A, _B], _, _) -> true;
four_of_a_kind([_B, A, A, A, A], _, _) -> true;
four_of_a_kind(_, _, _) -> false.

straight_flush([14, 5, 4, 3, 2], [S, S, S, S, S], Hand) -> {true, lists:keyreplace(14, 1, Hand, {1, S})};
straight_flush([E, D, C, B, A], [S, S, S, S, S], _) when A+1==B, B+1==C, C+1==D, D+1==E -> true;
straight_flush(_, _, _) -> false.
