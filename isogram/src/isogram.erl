-module(isogram).

-export([is_isogram/1]).


is_isogram(Phrase) ->
    TrimmedPhrase = re:replace(string:uppercase(Phrase), "-| ", "", [global, {return,list}]),
    Table = lists:foldl(fun(Ch, T)-> dict:update_counter(Ch, 1, T) end,
                        dict:new(),
                        TrimmedPhrase),
    dict:is_empty(
      dict:filter(fun(_K, V)-> V > 1 end, Table)).

