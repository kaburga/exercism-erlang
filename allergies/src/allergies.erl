-module(allergies).

-export([allergies/1, is_allergic_to/2]).

allergies() -> [eggs, peanuts, shellfish, strawberries, tomatoes, chocolate, pollen, cats].

allergie_values() ->
    Values = lists:seq(0, erlang:length(allergies()) - 1),
    lists:map(fun(N)-> erlang:trunc(math:pow(2, N)) end,
              Values).

allergies_with_values() ->
    Allergies = allergies(),
    AllergieValues = allergie_values(),
    lists:zip(Allergies, AllergieValues).

odd(I) -> (I >= 1) and (I rem 2 =/= 0).

allergies(Score) ->
    Allergies = allergies_with_values(),
    F = fun({Allergen, Val}, Acc)-> case odd(Score div Val) of
                                        true  -> Acc++[Allergen];
                                        false -> Acc
                                    end
        end,
    lists:foldl(F, [], Allergies).

is_allergic_to(Substance, Score) ->
    lists:member(Substance, allergies(Score)).

